﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main(string[] args)
        {
            var myName = "";
            var myAge = 0;

            Console.WriteLine("Please enter your name");
            myName = Console.ReadLine();

            Console.WriteLine("Please enter your age");
            myAge = int.Parse(Console.ReadLine());

            Console.WriteLine($"Hi my name is {myName} and my age is {myAge}");
        }
    }
}

